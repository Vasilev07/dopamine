# Rocket Builder
    This is not full implementation of the given task.
    This was implemented for couple of hours and need some improvments that would be listed in the next section.

## What was implemented:
    * Data from SpaceX API (See "Resources" below) must be obtained dynamically (not hardcoded)
    * 1 tone of fuel per second is consumed by each rocket
    * When all the fuel is depleted the rocket disappears
    * Code structure is Object Oriented
    * The project uses the provided project assets
    * Code uses ES6 features
    * Implements the usage of promises
    * App uses a library like EaselJS or PIXI.js or uses the Canvas API

## What should be implemented in next versions:
    * When all rockets have depleted their fuel a "Success" text message shows on the page
    * Implement rocket stages. When the fuel in first_stage is wasted the rocket should lose it's bottom half and continue on the second_stage fuel supply
    * The rocket should have exhaust flames shooting from the bottom while it moves
    * Rocket exhaust flames should be "pulsating" (mimicing real flames)
    * Implements the usage of events
    * Implements the usage of async/await
    * Implement a "REPLAY" button at the success screen
    * Rockets visualize the ammount of fuel left
    * Rockets fade out when their fuel is depleted

## Code optimisation:
    * Should split class usage in separate modules
    * Should implement endless screen