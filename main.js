const express = require('express');
const ejs = require('ejs');
const fs = require('fs');
const path = require('path');

const app = express();
const port = 3000;

app.use('/public', express.static(path.resolve(__dirname, 'public')));
app.get('/', (req, res) => { 
    res.sendFile(path.join(__dirname + '/playground.html'));
});

app.listen(port, () => console.log(`Example app listening at http://localhost:${port}`));